package com.mj.coupon.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.JavaType;

import com.mj.coupon.bean.CouponDto;
import com.mj.coupon.bean.CouponList;
import com.mj.coupon.util.HttpUtils;

import android.content.Context;

/**
 * 优惠劵
 * @author zhaominglei
 * @date 2015-7-5
 * 
 */
public class CouponService extends BaseService {
	@SuppressWarnings("unused")
	private static final String TAG = CouponService.class.getSimpleName();
	private static final String BRAND_SEARCH_INDEX_URL = "http://m.xixik.com/brand/"; //xixik
	private static final String BRAND_SEARCH_URL = "http://m.xixik.com/ajax.aspx"; //xixik
	private static final String KEYWORD_SEARCH_URL = "http://m.xixik.com/ajax/moreshow/"; //xixik
	private static final String KEYWORD_SEARCH_INDEX_URL = "http://m.xixik.com/search"; //xixik
	public static final String XIXIK_LIST_IMG_URL = "http://kfc.img.xixik.net/s/"; //xixik list
//	public static final String XIXIK_IMG_URL = "http://kfc.cache.xixik.net/s/"; //xixik
	public static final String XIXIK_DETAIL_IMG_URL = "http://kfc.img.xixik.net/c/"; //xixik detail
	
	public CouponList getTorrentList(Context context, String referer, String keyword, int page) {
		String searchURL = null;
		String from = null;
		String html = null;
		if (referer.equals("brand")) {
			searchURL = BRAND_SEARCH_URL+"?url="+keyword+"&pageindex="+page+"&type=morebrand&order=hot+desc&ordermodel=";
			from = BRAND_SEARCH_INDEX_URL+keyword+"/";
			html = HttpUtils.getBrandForXixik(searchURL, "gbk", from);
		} else if (referer.equals("query")) {
			searchURL = KEYWORD_SEARCH_URL+HttpUtils.encodeURI(keyword)+"/"+page+"/";
			from = KEYWORD_SEARCH_INDEX_URL+"?s="+HttpUtils.encodeURI(keyword);
			html = HttpUtils.getSearchForXixik(searchURL, "gbk", from);
		}
		if (html != null && !html.equals("")) {
			try {
				ObjectMapper mapper = new ObjectMapper();
				mapper.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true);
				mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true); 
				JavaType javaType = getCollectionType(mapper, ArrayList.class, CouponDto.class);
				@SuppressWarnings("unchecked")
				List<CouponDto> recordList = (List<CouponDto>)mapper.readValue(html, javaType);
				CouponList couponList = new CouponList();
				couponList.setRecordList(recordList);
				return couponList;
			} catch (JsonParseException e) {
				e.printStackTrace();
			} catch (JsonMappingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	 /**   
     * 获取泛型的Collection Type  
     * @param collectionClass 泛型的Collection   
     * @param elementClasses 元素类   
     * @return JavaType Java类型   
     */
	public static JavaType getCollectionType(ObjectMapper mapper, Class<?> collectionClass,	Class<?>... elementClasses) {
		return mapper.getTypeFactory().constructParametricType(collectionClass,	elementClasses);
	}
}
