package com.mj.coupon.activity;

import java.util.Timer;
import java.util.TimerTask;

import com.SYY.cbsdk.ui.AppControl;
import com.dyk.hfsdk.dao.util.DevListener;
import com.dyk.hfsdk.ui.Access;
import com.mj.coupon.R;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;
/**
 * 优惠劵
 * @author zhaominglei
 * @date 2015-7-4
 * 
 */
public class MainActivity extends Activity implements OnClickListener,DevListener {
	@SuppressWarnings("unused")
	private static final String TAG = MainActivity.class.getSimpleName();
	public static final String MUMAYI_APPID = "89f9f9dd4fd07d89eUjdUx+8Hiys5ElNCVkUC6+P4eMQoRM+tjTLdoeH8MXk5wWFVw"; //app_id
	public static final String MUMAYI_APPCHANNEL = "木蚂蚁"; //app_channel
	private boolean isExit = false;
	private TimerTask timerTask;
	private LinearLayout miniAdLinearLayout; //迷你广告
	private EditText couponText; //优惠劵输入框
	private String coupon; //优惠劵
	private Button queryBtn; //搜索
	private ImageView kfcView; //kfc
	private ImageView mdlView; //mdl
	private ImageView zgfView; //zgf
	private ImageView bskView; //bsk
	private ImageView yhdwView; //yhdw
	private ImageView ksfView; //ksf
	private ImageView xfyView; //xfy
	private ImageView ypsxView; //ypsx
	private ImageView jyjView; //jyj
	private ImageView hbwView; //hbw
	private ImageView dicosView; //dicos
	private ImageView dfjbView; //dfjb
	private Button appOffersButton; //推荐应用
	private Intent intent;
	private Access access;
	private AppControl appControl;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_main);
		init();
	}
	
	private void init() {
		miniAdLinearLayout = (LinearLayout)findViewById(R.id.miniAdLinearLayout);
		couponText = (EditText)findViewById(R.id.coupon_coupon_edt);
		kfcView = (ImageView)findViewById(R.id.logo_kfc); //kfc
		mdlView = (ImageView)findViewById(R.id.logo_mdl); //mdl
		zgfView = (ImageView)findViewById(R.id.logo_zgf); //zgf
		bskView = (ImageView)findViewById(R.id.logo_bsk); //bsk
		yhdwView = (ImageView)findViewById(R.id.logo_yhdw); //yhdw
		ksfView = (ImageView)findViewById(R.id.logo_ksf); //ksf
		xfyView = (ImageView)findViewById(R.id.logo_xfy); //xfy
		ypsxView = (ImageView)findViewById(R.id.logo_ypsx); //ypsx
		jyjView = (ImageView)findViewById(R.id.logo_jyj); //jyj
		hbwView = (ImageView)findViewById(R.id.logo_hbw); //hbw
		dicosView = (ImageView)findViewById(R.id.logo_dicos); //dicos
		dfjbView = (ImageView)findViewById(R.id.logo_dfjb); //dfjb
		queryBtn = (Button)findViewById(R.id.coupon_query_btn);
		appOffersButton = (Button)findViewById(R.id.appOffersButton);
		
		kfcView.setOnClickListener(this); //kfc
		mdlView.setOnClickListener(this); //mdl
		zgfView.setOnClickListener(this); //zgf
		bskView.setOnClickListener(this); //bsk
		yhdwView.setOnClickListener(this); //yhdw
		ksfView.setOnClickListener(this); //ksf
		xfyView.setOnClickListener(this); //xfy
		ypsxView.setOnClickListener(this); //ypsx
		jyjView.setOnClickListener(this); //jyj
		hbwView.setOnClickListener(this); //hbw
		dicosView.setOnClickListener(this); //dicos
		dfjbView.setOnClickListener(this); //dfjb
		queryBtn.setOnClickListener(this);
		appOffersButton.setOnClickListener(this);
		
		appControl = AppControl.getInstance();
		appControl.init(MainActivity.this, MUMAYI_APPID, MUMAYI_APPCHANNEL);
		appControl.loadPopAd(MainActivity.this);
		appControl.showPopAd(MainActivity.this, 60 * 1000);
		appControl.showInter(MainActivity.this, miniAdLinearLayout);
		
		access = Access.getInstance();
		// 初始化带player_id
		access.init(MainActivity.this, MUMAYI_APPID, MUMAYI_APPCHANNEL);
		// 设置初始积分                                                              
		access.setdefaultSCORE(this, 100);
		// 设置获取积分的监听
		access.setAppListener(this, this);
	}
	
	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.coupon_query_btn:
			coupon = couponText.getText().toString();
			if (coupon == null || coupon.equals("")) {
				Toast.makeText(getApplicationContext(), R.string.coupon_coupon_hint, Toast.LENGTH_SHORT).show();
				return;
			}
			intent = new Intent(MainActivity.this, CouponListActivity.class);
			intent.putExtra("referer", "query");
			intent.putExtra("keyword", coupon);
			startActivity(intent);
			break;
			
		case R.id.logo_kfc:
			intent = new Intent(MainActivity.this, CouponListActivity.class);
			intent.putExtra("referer", "brand");
			intent.putExtra("keyword", "kfc");
			startActivity(intent);
			break;
		case R.id.logo_mdl:
			intent = new Intent(MainActivity.this, CouponListActivity.class);
			intent.putExtra("referer", "brand");
			intent.putExtra("keyword", "mdl");
			startActivity(intent);
			break;
		case R.id.logo_zgf:
			intent = new Intent(MainActivity.this, CouponListActivity.class);
			intent.putExtra("referer", "brand");
			intent.putExtra("keyword", "zgf");
			startActivity(intent);
			break;
		case R.id.logo_bsk:
			intent = new Intent(MainActivity.this, CouponListActivity.class);
			intent.putExtra("referer", "brand");
			intent.putExtra("keyword", "bsk");
			startActivity(intent);
			break;
		case R.id.logo_yhdw:
			intent = new Intent(MainActivity.this, CouponListActivity.class);
			intent.putExtra("referer", "brand");
			intent.putExtra("keyword", "yhdw");
			startActivity(intent);
			break;
		case R.id.logo_ksf:
			intent = new Intent(MainActivity.this, CouponListActivity.class);
			intent.putExtra("referer", "brand");
			intent.putExtra("keyword", "ksf");
			startActivity(intent);
			break;
		case R.id.logo_xfy:
			intent = new Intent(MainActivity.this, CouponListActivity.class);
			intent.putExtra("referer", "brand");
			intent.putExtra("keyword", "xfy");
			startActivity(intent);
			break;
		case R.id.logo_ypsx:
			intent = new Intent(MainActivity.this, CouponListActivity.class);
			intent.putExtra("referer", "brand");
			intent.putExtra("keyword", "ypsx");
			startActivity(intent);
			break;
		case R.id.logo_jyj:
			intent = new Intent(MainActivity.this, CouponListActivity.class);
			intent.putExtra("referer", "brand");
			intent.putExtra("keyword", "jyj");
			startActivity(intent);
			break;
		case R.id.logo_hbw:
			intent = new Intent(MainActivity.this, CouponListActivity.class);
			intent.putExtra("referer", "brand");
			intent.putExtra("keyword", "hbw");
			startActivity(intent);
			break;
		case R.id.logo_dicos:
			intent = new Intent(MainActivity.this, CouponListActivity.class);
			intent.putExtra("referer", "brand");
			intent.putExtra("keyword", "dicos");
			startActivity(intent);
			break;
		case R.id.logo_dfjb:
			intent = new Intent(MainActivity.this, CouponListActivity.class);
			intent.putExtra("referer", "brand");
			intent.putExtra("keyword", "dfjb");
			startActivity(intent);
			break;
		
		case R.id.appOffersButton:
			access.openWALL(MainActivity.this);
			break;
			
		default:
			break;
		}
	}
	
	@Override
	public void onBackPressed() {
		if (isExit) {
			MainActivity.this.finish();
		} else {
			isExit = true;
			Toast.makeText(MainActivity.this, R.string.exit_msg, Toast.LENGTH_SHORT).show();
			timerTask = new TimerTask() {
				@Override
				public void run() {
					isExit = false;
				}
			};
			new Timer().schedule(timerTask, 2*1000);
		}
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		appControl.close(this);
	}
	
	@Override
	public void onDevSucceed(int eggs) {
		System.out.println("积分获取成功:" + eggs);
	}

	@Override
	public void onDevFailed(String message) {
		System.out.println("积分获取失败:" + message);
	}

	@Override
	public void onClose(Context context) {
		((Activity) context).finish();
	}	
}
