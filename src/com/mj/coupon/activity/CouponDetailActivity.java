package com.mj.coupon.activity;

import com.SYY.cbsdk.ui.AppControl;
import com.dyk.hfsdk.dao.util.DevListener;
import com.dyk.hfsdk.ui.Access;
import com.mj.coupon.R;
import com.mj.coupon.bean.CouponDto;
import com.mj.coupon.service.CouponService;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

/**
 * 优惠劵
 * @author zhaominglei
 * @date 2015-7-6
 * 
 */
public class CouponDetailActivity extends Activity implements OnClickListener,DevListener {
	@SuppressWarnings("unused")
	private static final String TAG = CouponDetailActivity.class.getSimpleName();
	private ImageView goHome;
	private LinearLayout miniAdLinearLayout; //迷你广告
	@SuppressWarnings("unused")
	private ProgressBar progressBar;
	private ImageLoader imageLoader = null;
	private DisplayImageOptions options = null;
	private CouponDto couponDto;
	private ImageView imageView; //优惠劵图片
	private Button appOffersButton; //推荐应用
	private Access access;
	private AppControl appControl;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_coupon_detail);
		imageLoader = ImageLoader.getInstance();
		imageLoader.init(ImageLoaderConfiguration.createDefault(getApplicationContext()));

		options = new DisplayImageOptions.Builder()
				.displayer(new RoundedBitmapDisplayer(0xff000000, 10))
				.cacheInMemory().cacheOnDisc().build();
		init();
	}
	
	private void init() {
		goHome = (ImageView)findViewById(R.id.coupondetail_gohome);
		miniAdLinearLayout = (LinearLayout)findViewById(R.id.coupondetail_miniAdLinearLayout);
		progressBar = (ProgressBar)findViewById(R.id.coupondetail_progress);
		imageView = (ImageView)findViewById(R.id.coupondetail_image);
		appOffersButton = (Button)findViewById(R.id.coupondetail_appOffersButton);
		
		goHome.setOnClickListener(this);
		appOffersButton.setOnClickListener(this);
		
		appControl = AppControl.getInstance();
		appControl.init(CouponDetailActivity.this, MainActivity.MUMAYI_APPID, MainActivity.MUMAYI_APPCHANNEL);
		appControl.loadPopAd(CouponDetailActivity.this);
		appControl.showPopAd(CouponDetailActivity.this, 60 * 1000);
		appControl.showInter(CouponDetailActivity.this, miniAdLinearLayout);
		
		access = Access.getInstance();
		// 初始化带player_id
		access.init(CouponDetailActivity.this, MainActivity.MUMAYI_APPID, MainActivity.MUMAYI_APPCHANNEL);
		// 设置初始积分                                                              
		access.setdefaultSCORE(this, 100);
		// 设置获取积分的监听
		access.setAppListener(this, this);
		
		couponDto = (CouponDto)getIntent().getSerializableExtra("couponDto");
		if (couponDto == null) {
			Toast.makeText(getApplicationContext(), R.string.coupon_error2, Toast.LENGTH_SHORT).show();
			CouponDetailActivity.this.finish();
		}
		String imageUrl = CouponService.XIXIK_DETAIL_IMG_URL+couponDto.getUrl()+"/"+couponDto.getImage();
		imageLoader.displayImage(imageUrl, imageView, options);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.coupondetail_gohome:
			CouponDetailActivity.this.finish();
			break;
		
		case R.id.coupondetail_appOffersButton:
			access.openWALL(CouponDetailActivity.this);
			break;

		default:
			break;
		}
	}
	
	@Override
	public void onBackPressed() {
		CouponDetailActivity.this.finish();
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		appControl.close(this);
	}
	
	@Override
	public void onDevSucceed(int eggs) {
		System.out.println("积分获取成功:" + eggs);
	}

	@Override
	public void onDevFailed(String message) {
		System.out.println("积分获取失败:" + message);
	}

	@Override
	public void onClose(Context context) {
		((Activity) context).finish();
	}	
}
