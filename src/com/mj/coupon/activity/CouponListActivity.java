package com.mj.coupon.activity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.SYY.cbsdk.ui.AppControl;
import com.dyk.hfsdk.dao.util.DevListener;
import com.dyk.hfsdk.ui.Access;
import com.mj.coupon.R;
import com.mj.coupon.adapter.CouponAdapter;
import com.mj.coupon.bean.CouponDto;
import com.mj.coupon.bean.CouponList;
import com.mj.coupon.service.CouponService;
import com.mj.coupon.swipeback.SwipeBackActivity;
import com.mj.coupon.util.DateUtils;
import com.mj.coupon.util.NetUtils;
import com.mj.coupon.widget.LoadingDialog;
import com.mj.coupon.widget.XListView;
import com.mj.coupon.widget.XListView.IXListViewListener;

/**
 * 优惠劵列表
 * @author zhaominglei
 * @date 2015-7-5
 * 
 */
public class CouponListActivity extends SwipeBackActivity implements OnClickListener,IXListViewListener,DevListener {
	@SuppressWarnings("unused")
	private static final String TAG = CouponListActivity.class.getSimpleName();
	private CouponService couponService = new CouponService();
	private ImageView goHome;
	private LinearLayout miniAdLinearLayout; //迷你广告
	private LoadingDialog loadingDialog; //加载框
	private XListView couponListView;
	private List<CouponDto> couponDtos = new ArrayList<CouponDto>();
	private	CouponAdapter couponAdapter;
	private Button appOffersButton; //推荐应用
	private int page = 1; //页码
	private String referer; //来自那个链接
	private String keyword; //关键词
	private Access access;
	private AppControl appControl;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_coupon_list);
		init();
	}
	
	private void init() {
		goHome = (ImageView)findViewById(R.id.couponlist_gohome);
		miniAdLinearLayout = (LinearLayout)findViewById(R.id.couponlist_miniAdLinearLayout);
		couponListView = (XListView)findViewById(R.id.couponlist_listview);
		couponAdapter = new CouponAdapter(this, couponDtos);
		couponListView.setPullLoadEnable(true);
		couponListView.setXListViewListener(this);
		couponListView.setAdapter(couponAdapter);
		appOffersButton = (Button)findViewById(R.id.couponlist_appOffersButton);
		
		goHome.setOnClickListener(this);
		appOffersButton.setOnClickListener(this);
		loadingDialog = new LoadingDialog(this);
		
		appControl = AppControl.getInstance();
		appControl.init(CouponListActivity.this, MainActivity.MUMAYI_APPID, MainActivity.MUMAYI_APPCHANNEL);
		appControl.loadPopAd(CouponListActivity.this);
		appControl.showPopAd(CouponListActivity.this, 60 * 1000);
		appControl.showInter(CouponListActivity.this, miniAdLinearLayout);
		
		access = Access.getInstance();
		// 初始化带player_id
		access.init(CouponListActivity.this, MainActivity.MUMAYI_APPID, MainActivity.MUMAYI_APPCHANNEL);
		// 设置初始积分                                                              
		access.setdefaultSCORE(this, 100);
		// 设置获取积分的监听
		access.setAppListener(this, this);
		
		referer = getIntent().getStringExtra("referer");
		keyword = getIntent().getStringExtra("keyword");
		if (keyword == null || keyword.equals("") || referer == null || referer.equals("")) {
			Toast.makeText(getApplicationContext(), R.string.coupon_error1, Toast.LENGTH_SHORT).show();
			CouponListActivity.this.finish();
		}
		getCouponList();
	}
	
	private void getCouponList() {
		if (NetUtils.isConnected(getApplicationContext())) {
			loadingDialog.show();
			new CouponListAsyncTask().execute("");
		} else {
			Toast.makeText(getApplicationContext(), R.string.net_error, Toast.LENGTH_SHORT).show();
			onLoad();
		}
	}
	
	@Override
	public void onRefresh() {
		getCouponList();
	}

	@Override
	public void onLoadMore() {
		getCouponList();
		page++;
	}
	
	//刷新结束
	public void onLoad() {
		couponListView.stopRefresh();
		couponListView.stopLoadMore();
		couponListView.setRefreshTime(DateUtils.format(new Date(), DateUtils.datePattern));
	}	

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.couponlist_gohome:
			CouponListActivity.this.finish();
			break;
		
		case R.id.couponlist_appOffersButton:
			access.openWALL(CouponListActivity.this);
			break;
			
		default:
			break;
		}
	}
	
	@Override
	public void onBackPressed() {
		CouponListActivity.this.finish();
	}
	
	public class CouponListAsyncTask extends AsyncTask<String, String, CouponList> {
		@Override
		protected CouponList doInBackground(String... params) {
			return couponService.getTorrentList(getApplicationContext(), referer, keyword, page);
		}
		@Override
		protected void onPostExecute(CouponList result) {
			loadingDialog.dismiss();
			if (result != null && result.getRecordList() != null && result.getRecordList().size() > 0) {
				couponAdapter.setCouponDtos(result.getRecordList());
				couponAdapter.notifyDataSetChanged();
				onLoad();
			}
		}
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		appControl.close(this);
	}
	
	@Override
	public void onDevSucceed(int eggs) {
		System.out.println("积分获取成功:" + eggs);
	}

	@Override
	public void onDevFailed(String message) {
		System.out.println("积分获取失败:" + message);
	}

	@Override
	public void onClose(Context context) {
		((Activity) context).finish();
	}	
}
