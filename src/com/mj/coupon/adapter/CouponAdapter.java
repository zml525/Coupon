package com.mj.coupon.adapter;

import java.util.List;

import com.mj.coupon.R;
import com.mj.coupon.activity.CouponDetailActivity;
import com.mj.coupon.bean.CouponDto;
import com.mj.coupon.service.CouponService;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * 优惠劵列表
 * @author zhaominglei
 * @date 2015-7-5
 * 
 */
public class CouponAdapter extends BaseAdapter {
	private Context context;
	private List<CouponDto> couponDtos;
	@SuppressWarnings("unused")
	private ProgressBar progressBar;
	private ImageLoader imageLoader = null;
	private DisplayImageOptions options = null;

	public CouponAdapter(Context context, List<CouponDto> couponDtos) {
		super();
		this.context = context;
		this.couponDtos = couponDtos;
		imageLoader = ImageLoader.getInstance();
		imageLoader.init(ImageLoaderConfiguration.createDefault(context));

		options = new DisplayImageOptions.Builder()
				.displayer(new RoundedBitmapDisplayer(0xff000000, 10))
				.cacheInMemory().cacheOnDisc().build();
	}

	public List<CouponDto> getCouponDtos() {
		return couponDtos;
	}

	public void setCouponDtos(List<CouponDto> couponDtos) {
		this.couponDtos = couponDtos;
	}

	@Override
	public int getCount() {
		return couponDtos.size();
	}

	@Override
	public Object getItem(int position) {
		return couponDtos.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		RelativeLayout relativeLayout;
		if (convertView != null) {
			relativeLayout = (RelativeLayout) convertView;
		} else {
			relativeLayout = (RelativeLayout) View.inflate(context,
					R.layout.coupon_item, null);
		}
		progressBar = (ProgressBar) relativeLayout.findViewById(R.id.coupon_item_progress);
		ImageView imageView = (ImageView) relativeLayout.findViewById(R.id.coupon_item_image);

		final CouponDto couponDto = couponDtos.get(position);
		String imageUrl = CouponService.XIXIK_LIST_IMG_URL+couponDto.getUrl()+"/"+couponDto.getImage();
		imageLoader.displayImage(imageUrl, imageView, options);
		TextView nameTxt = ((TextView) relativeLayout.findViewById(R.id.coupon_item_template_name));
		TextView priceTxt = ((TextView)relativeLayout.findViewById(R.id.coupon_item_template_price));
		TextView couponPriceTxt = ((TextView)relativeLayout.findViewById(R.id.coupon_item_template_coupon_price));
		TextView enddateTxt = ((TextView)relativeLayout.findViewById(R.id.coupon_item_template_enddate));
		
		String name = couponDto.getTitle2().replaceAll("嘻嘻网", "");
		nameTxt.setText(name);
		if (couponDto.getPrice() != null && !couponDto.getPrice().equals("")) {
			priceTxt.setText(couponDto.getPrice()+"元");
		}
		if (couponDto.getCoupon_price() != null && !couponDto.getCoupon_price().equals("")) {
			couponPriceTxt.setText("节省了"+couponDto.getCoupon_price()+"元");
		} else {
			couponPriceTxt.setVisibility(View.INVISIBLE);
		}
		String enddate = "截至"+couponDto.getEndtime().replaceAll(" 0:00:00", "");
		enddateTxt.setText(enddate);
		relativeLayout.setTag(couponDto.getId());
		relativeLayout.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String descId = (String)v.getTag();
				if (descId != null && descId.equals(couponDto.getId())) {
					Intent intent = new Intent(v.getContext(), CouponDetailActivity.class);
					Bundle bundle = new Bundle();
					bundle.putSerializable("couponDto", couponDto);
					intent.putExtras(bundle);
					v.getContext().startActivity(intent);
				}
			}
		});
		return relativeLayout;
	}
}
